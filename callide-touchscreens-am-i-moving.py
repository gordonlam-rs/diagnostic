#
# Load the Pandas libraries with alias 'pd'
import pandas as pd
import plotly.express as px
import matplotlib.dates as dates
import matplotlib.pyplot as plt


def inspect(rows):
    rows['start_time'] = rows.timestamp
    rows['end_time'] = rows.timestamp
    print(rows)


# Read data from file 'filename.csv'
# (in the same directory that your python process is based)
# Control delimiters, rows, column names with read_csv (see later)
data = pd.read_csv("callide_screen_blocker_investigation.csv", parse_dates=True)
# Preview the first 5 lines of the loaded data

data.info()
grouped = data.groupby("equipment_site_name")

for group in grouped.groups:
    sorted = grouped.get_group(group).sort_values(by=['timestamp'])
    for row in sorted.iterrows():
        print(row)


# plot_data = pd.DataFrame({'start': dates.date2num(pd.to_datetime(data.min().timestamp)), 'stop':  dates.date2num(pd.to_datetime(data.max().timestamp))}, data.min().index)
#
#
# fig = px.timeline(data, x_start="timestamp", x_end="timestamp",y="equipment_site_name", color="status")
# fig.show()




# import random
#
# for x in range(0, 10, 2):
#     color = random.choice(['red', 'green', 'blue', 'yellow'])
#     plt.hlines(1, x, x + 2, colors=color, lw=5)
#     plt.text(x + 1, 1.01, color, ha='center')
# plt.show()
