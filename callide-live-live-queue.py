import logging
import threading
import sys
import json
import time
from geopy import distance

import psycopg2
import requests as requests
from requests.auth import HTTPBasicAuth
from dotenv import dotenv_values
from datetime import datetime
import pytz

from bokeh.plotting import figure
from bokeh.models import ColumnDataSource, DataTable, DateFormatter, TableColumn
from tornado.ioloop import IOLoop
from bokeh.server.server import Server
from bokeh.application import Application
from bokeh.application.handlers.function import FunctionHandler
from bokeh.layouts import column, row

logging.basicConfig(level=logging.DEBUG)
formatter = logging.Formatter('%(asctime)s - %(name)s - %(levelname)s - %(message)s')
log = logging.getLogger()
# streamHandler = logging.StreamHandler(sys.stdout)
# streamHandler.setFormatter(formatter)
# log.addHandler(streamHandler)

config = dotenv_values("postgres.database.env")
POSTGRES_CONNECTION = f"postgres://{config['POSTGRES_USER']}:{config['POSTGRES_PASSWORD']}@localhost:6432/{config['POSTGRES_DB']}"

global database_connection


def set_up_postgres_connection():
    try:
        conn = psycopg2.connect(POSTGRES_CONNECTION)
        return conn
    except:
        raise "I am unable to connect to the database"


def start_fetch_equipment_data(db_conn, delay):
    try:
        while True:
            time.sleep(delay)
            fetch_and_store_equipment_data(db_conn)

    except KeyboardInterrupt:
        logging.info("exiting")
        sys.exit()


def create_equipment_data_table(db_conn):
    cursor = db_conn.cursor()
    log.info("Creating equipment data table")
    try:
        query_create_equipment_data_table = """CREATE TABLE IF NOT EXISTS equipment_location_data (
                                           write_time TIMESTAMPTZ NOT NULL,
                                           equipment_site_name text NOT NULL,
                                           equipment_class text NOT NULL,
                                           location_time_stamp TIMESTAMPTZ NOT NULL,
                                           latitude DOUBLE PRECISION,
                                           longitude DOUBLE PRECISION,
                                           movement_state text NOT NULL
                                           );"""
        cursor.execute(query_create_equipment_data_table)
        query_create_equipment_data_hypertable = "SELECT create_hypertable('equipment_location_data', 'location_time_stamp', " \
                                                 "chunk_time_interval => 86400000000, if_not_exists => TRUE); "
        cursor.execute(query_create_equipment_data_hypertable)
    except:
        raise "Cannot create equipment data table"
    db_conn.commit()
    cursor.close()


def fetch_and_store_equipment_data(db_conn):
    cursor = db_conn.cursor()
    try:
        with open("/home/gordonlam/.maxmine-credentials.json", "r") as credsFile:
            creds = json.load(credsFile)

        log.info("Fetching equipment data")
        max_creds = creds["maxmineAPIs"]
        response = requests.get('https://api.callide.batchfire.reporting.max-mine.com/fms/truck-allocation/equipment',
                                auth=HTTPBasicAuth(max_creds['username'], max_creds['password']))
        all_equipment = response.json()["features"]

        for equipment in all_equipment:
            properties = equipment['properties']
            equipment_details = properties['equipment']
            position = equipment_details['position']
            if position is None or position['last-update'] is None:
                continue
            location_entry_timestamp = position['last-update']

            utc_dt = datetime.utcfromtimestamp(location_entry_timestamp).strftime('%Y-%m-%d %H:%M:%S')
            # timescale DB query execution
            insert_query = "INSERT INTO equipment_location_data (write_time, equipment_site_name, equipment_class, location_time_stamp, latitude, longitude, movement_state) VALUES (%s, %s, %s, %s, %s, %s, %s)"
            record_to_insert = (
                "now()", equipment_details['site-name'], equipment_details['class'],
                utc_dt,
                position['latitude'], position['longitude'], equipment_details['activity'])
            cursor.execute(insert_query, record_to_insert)
    except:
        log.warning("cannot insert equipement location data")

    db_conn.commit()
    cursor.close()


def create_the_latest_view_on_equipment_location_data_of_loaders(db_conn):
    cursor = db_conn.cursor()
    log.info("Creating loaders location view on latest reading")
    # creating a view that return all the latest location and status of trucks in last 5 minutes
    # put as 5 minutes as the loaders usually have bad reception
    try:
        query_drop_loaders_location_view = "DROP VIEW IF EXISTS loaders_latest_location_data"
        query_create_loaders_location_view = """CREATE VIEW loaders_latest_location_data 
                                               AS 
                                               select
                                               equipment_site_name,
                                               latitude, 
                                               longitude, 
                                                location_time_stamp
                                                from (
                                               select 
                                                equipment_site_name, 
                                                latitude, longitude, 
                                                location_time_stamp, 
                                                row_number() over (partition by equipment_site_name order by location_time_stamp DESC) as rownum
                                                from equipment_location_data
                                                WHERE equipment_class='Load Unit') tmp where rownum = 1 AND location_time_stamp > NOW() - interval '5 minutes';;
                                               """
        cursor.execute(query_drop_loaders_location_view)
        cursor.execute(query_create_loaders_location_view)
    except:
        raise "Cannot create loaders location view on latest reading"
    db_conn.commit()
    cursor.close()


def select_the_latest_view_on_equipment_location_data_of_loaders(db_conn):
    cursor = db_conn.cursor()
    log.info("Selecting loaders location view on latest reading")
    try:
        query_select_loaders_location_view = """
         select * from loaders_latest_location_data;
                                               """
        cursor.execute(query_select_loaders_location_view)
        return cursor.fetchall()
    except:
        raise "Cannot select loaders location view on latest reading"


def create_the_latest_view_on_equipment_location_data_of_trucks(db_conn):
    cursor = db_conn.cursor()
    log.info("Creating trucks location view on latest reading")
    # creating a view that return all the latest location and status of trucks in last 3 minutes
    try:
        query_drop_trucks_location_view = "DROP VIEW IF EXISTS trucks_latest_location_data"
        query_create_trucks_location_view = """CREATE VIEW trucks_latest_location_data 
                                               AS 
                                               select
                                               equipment_site_name,
                                               latitude, 
                                               longitude, 
                                                location_time_stamp,
                                                movement_state
                                                from (
                                               select 
                                                equipment_site_name, 
                                                latitude, longitude, 
                                                location_time_stamp, 
                                                movement_state,
                                                row_number() over (partition by equipment_site_name order by location_time_stamp DESC) as rownum
                                                from equipment_location_data
                                                WHERE equipment_class='Haul Truck') tmp where rownum = 1 AND location_time_stamp > NOW() - interval '3 minutes';;
                                               """
        cursor.execute(query_drop_trucks_location_view)
        cursor.execute(query_create_trucks_location_view)
    except:
        raise "Cannot create trucks location view on latest reading"
    db_conn.commit()
    cursor.close()


def select_the_latest_view_on_equipment_location_data_of_trucks(db_conn):
    cursor = db_conn.cursor()
    log.info("Selecting trucks location view on latest reading")
    try:
        query_select_trucks_location_view = """
         select * from trucks_latest_location_data;
                                               """
        cursor.execute(query_select_trucks_location_view)
        return cursor.fetchall()
    except:
        raise "Cannot select trucks location view on latest reading"


def make_document(doc):
    loader_longitude = []
    loader_latitude = []
    moving_truck_longitude = []
    moving_truck_latitude = []
    stationary_truck_longitude = []
    stationary_truck_latitude = []
    queueing_truck_longitude = []
    queueing_truck_latitude = []
    loader_locations_source = ColumnDataSource(data=dict(longitude=loader_longitude, latitude=loader_latitude))
    moving_truck_locations_source = ColumnDataSource(
        data=dict(longitude=moving_truck_longitude, latitude=moving_truck_latitude))
    stationary_truck_locations_source = ColumnDataSource(
        data=dict(longitude=stationary_truck_longitude, latitude=stationary_truck_latitude))
    queueing_truck_locations_source = ColumnDataSource(
        data=dict(longitude=queueing_truck_longitude, latitude=queueing_truck_latitude))

    loaders_have_queue = []
    trucks_in_queue = []

    queue_table = ColumnDataSource(data=dict(loaders=loaders_have_queue, trucks=trucks_in_queue))

    def update():
        # don't look into this function not sure what I am doing here!
        loader_locations = select_the_latest_view_on_equipment_location_data_of_loaders(database_connection)
        truck_locations = select_the_latest_view_on_equipment_location_data_of_trucks(database_connection)

        moving_truck_locations = [i for i in truck_locations if i[4] == 'Moving']
        stationary_truck_locations = [i for i in truck_locations if i[4] == 'Stationary']
        queueing_truck_locations = []

        # reform the queue data table
        loaders_have_queue = []
        trucks_in_queue = []
        for index, loader_location in enumerate(loader_locations):
            loaders_have_queue.append(loader_location[0])
            queue = []
            for truck_location in stationary_truck_locations:
                # calculate all the distance between the loaders and trucks bit slow and sucky
                distance_of = distance.distance((truck_location[1], truck_location[2]),
                                                (loader_location[1], loader_location[2]))
                #  only counting the trucks within 200 meters
                if distance_of < 0.2:
                    queue.append((truck_location[0], distance_of))
            queue.sort(key=lambda x:x[1]) # sort them by distance
            queue = [q[0] for q in queue] # only gets the name
            trucks_in_queue.append(queue)
        queue_table.data = dict(loaders=loaders_have_queue, trucks=trucks_in_queue)

        loader_longitude = [d[1] for d in loader_locations]
        loader_latitude = [d[2] for d in loader_locations]

        moving_truck_longitude = [d[1] for d in moving_truck_locations]
        moving_truck_latitude = [d[2] for d in moving_truck_locations]
        stationary_truck_longitude = [d[1] for d in stationary_truck_locations]
        stationary_truck_latitude = [d[2] for d in stationary_truck_locations]
        queueing_truck_longitude = [d[1] for d in queueing_truck_locations]
        queueing_truck_latitude = [d[2] for d in queueing_truck_locations]

        loader_locations_source.data = dict(longitude=loader_longitude, latitude=loader_latitude)
        moving_truck_locations_source.data = dict(longitude=moving_truck_longitude, latitude=moving_truck_latitude)
        stationary_truck_locations_source.data = dict(longitude=stationary_truck_longitude,
                                                      latitude=stationary_truck_latitude)
        queueing_truck_locations_source.data = dict(longitude=queueing_truck_longitude,
                                                    latitude=queueing_truck_latitude)

    p = figure(plot_width=500, plot_height=500)
    p.circle(y='longitude', x='latitude', source=loader_locations_source)
    p.cross(y='longitude', x='latitude', source=moving_truck_locations_source, color="#FF0000")
    p.cross(y='longitude', x='latitude', source=stationary_truck_locations_source, color="#00FF00")
    p.square_pin(y='longitude', x='latitude', source=queueing_truck_locations_source, color="#0000FF")

    queue_data_table_columns = [
        TableColumn(field='loaders', title='loaders'),
        TableColumn(field='trucks', title='trucks'),
    ]

    queue_data_table = DataTable(source=queue_table, columns=queue_data_table_columns)

    layout = column(row(p), row(queue_data_table))
    doc.add_periodic_callback(update, 1000)
    doc.add_root(layout)


if __name__ == "__main__":
    postgres_conn = set_up_postgres_connection()
    database_connection = postgres_conn
    # create_equipment_table(postgres_conn)
    create_equipment_data_table(postgres_conn)

    # fetch the data every
    t1 = threading.Thread(target=start_fetch_equipment_data, args=[database_connection, 5])
    t1.start()

    create_the_latest_view_on_equipment_location_data_of_loaders(database_connection)
    create_the_latest_view_on_equipment_location_data_of_trucks(database_connection)
    io_loop = IOLoop.current()
    server = Server({'/myapp': Application(FunctionHandler(make_document))}, port=5001, io_loop=io_loop)
    server.start()
    server.show('/myapp')
    io_loop.start()
