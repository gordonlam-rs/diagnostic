from elasticsearch import Elasticsearch
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np


def get_all_touchscreen_chunks():
    es = Elasticsearch(
        [
            'https://gordon.lam@resolution.systems:bB9fnlTjQ32vhO1jRrWTIK2v@ad2ffc6c3c93418498551fcc6721af39.ap-southeast-2.aws.found.io:9243']
    )

    ES_SCROLL_TIME = '1s'

    query_body = {
        "bool": {
            "must": [
                {
                    "match": {
                        "minesite_id": "batchfire-callide"
                    }
                },
                {
                    "match": {
                        "type": "delay_capture"
                    }
                },
                {
                    "range": {
                        "@timestamp": {
                            "gte": "now-15d",
                            "lte": "now"
                        }
                    }
                },
                {
                    "match": {
                        "in_production": "true"
                    }
                },
            ]
        }
    }

    result = es.search(
        index="touchscreen-operator-*",
        query=query_body,
        size=10000,
        scroll=ES_SCROLL_TIME  # the time of search context keeping
    )

    last_scroll_id = result['_scroll_id']

    all_chunks = []
    all_hits = result["hits"]["hits"]
    # iterate the nested dictionaries inside the ["hits"]["hits"] list
    for num, doc in enumerate(all_hits):
        all_chunks.append(doc["_source"])

    # continuous scrolling until there is no data in ES
    while len(result['hits']['hits']):
        result = es.scroll(
            scroll_id=last_scroll_id,
            scroll=ES_SCROLL_TIME  # length of time to keep search context
        )
        # update the scroll id
        last_scroll_id = result['_scroll_id']

        all_hits = result["hits"]["hits"]
        # iterate the nested dictionaries inside the ["hits"]["hits"] list
        for num, doc in enumerate(all_hits):
            all_chunks.append(doc["_source"])

    return all_chunks


if __name__ == "__main__":
    chunks = get_all_touchscreen_chunks()
    df = pd.DataFrame(chunks, columns=['data', 'equipment_site_name', 'logger_time_ms', 'uploaded_to_s3_ts'])

    # round time to ms then to datetime
    df['logger_time_ms'] = pd.to_datetime(df['logger_time_ms'], unit='ms')
    # the ms was stripped
    df['uploaded_to_s3_ts'] = pd.to_datetime(df['uploaded_to_s3_ts'] * 1000, unit='ms')

    # calculate the time diff between the upload time and event time
    df['time_delta'] = (df['uploaded_to_s3_ts'] - df['logger_time_ms']).dt.total_seconds().astype(int)
    maxTimeDelta = df['time_delta'].max()
    minTimeDelta = df['time_delta'].min()

    df.hist(column='time_delta', bins=range(minTimeDelta, maxTimeDelta, 5000))

    plt.show()
